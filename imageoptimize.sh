#!/bin/bash
optimize() {
  jpegoptim *.jpg --strip-all --all-progressive
  optipng -o2 -strip all *.png
  for i in *
  do
    if test -d $i
    then
      cd $i
      echo $i
      optimize
      cd ..
    fi
  done
  echo
}
optimize